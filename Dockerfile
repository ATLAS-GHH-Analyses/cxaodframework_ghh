FROM atlas/analysisbase:21.2.166
ADD . /analysis/source
WORKDIR /analysis/build
RUN source ~/release_setup.sh &&  \
    sudo chown -R atlas /analysis && \
    mkdir ../run && \
    cmake ../source && \
    make -j4

# Lines needed to run with reana
USER root
RUN sudo usermod -G root atlas
USER atlas

